import spacy  # type: ignore
import logging
from json import dumps
import fastapi
from typing import Dict

logging.info("Loading model..")
nlp = spacy.load("./models")
app = fastapi.FastAPI()


@app.get("/api/intent")
def intent_inference(sentence: str) -> Dict[str, float]:
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return inference.cats


@app.get("/api/intent-supported-languages")
def supported_languages():
    return dumps(["fr-FR"])


@app.get("/health", status_code=200)
def health_check_endpoint():
    pass
