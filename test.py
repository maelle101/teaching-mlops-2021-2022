import main


def test_support_french():
    assert main.supported_languages() == "[\"fr-FR\"]"
