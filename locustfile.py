import time

from locust import FastHttpUser, task, stats                # type: ignore
from locust.runners import STATE_STOPPED, STATE_STOPPING    # type: ignore

stats.PERCENTILES_TO_REPORT = [0.99]


class MyUser(FastHttpUser):
    @task
    def request(self):
        state = self.environment.runner.state
        if state not in [STATE_STOPPED, STATE_STOPPING]:
            self.client.get("/api/intent?sentence=restaurant")
            environment = self.environment
            stat = environment.stats \
                .get("/api/intent?sentence=restaurant", 'GET') \
                .get_current_response_time_percentile(0.99)

            stat = stat if stat is not None else 0
            if stat > 200:
                print(f"99th percentile for {environment.runner.user_count} \
                 users: {stat}ms")
                time.sleep(1)
                environment.runner.stop()
                return
