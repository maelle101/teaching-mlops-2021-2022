FROM python:3.8-slim-buster #python:3.7-buster
COPY requirements.txt ./

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git && \
    apt-get install -y ruby-dev && \
    apt-get install -y curl
RUN pip install --upgrade pip && pip install -r ./requirements.txt
RUN poetry install --no-dev
