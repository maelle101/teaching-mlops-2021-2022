# TP 3A IA : MLOps : Serving & DevOps + Python avancé: WebServices

## Fonctionnalités implémentées
- L'API est passée sur le framework FastApi
- L'étape *docker* de la CI/CD génère une image Docker en exécutant :
    - Soit le job *docker* pour générer une image puis le job *docker_publish* pour l'enregister
    - Soit le job *docker_reuse* dans le cas ou une image générée par une précédente exécution de la pipeline est
      réutilisable
- L'étape *test* effectue parallèlement deux jobs:
    - *test_job* qui effectue un test unitaire avec le framework pytest
    - *code_quality_job* qui effectue un linting avec les framework mypy et pycodestyle. L'échec de cette étape ne
      stoppe pas l'exécution de la pipeline.
- Ls dépendance entre les jobs sont gérées pour permettre l'exécution parallèle des tâches ne dépendant pas les unes des
  autres (Voir schéma ci-dessous)

![Visualisation de la CI/CD](cicd_scheme.png)
## Fonctionnalités non implémentées
- L'étape de déploiement sur Heroku ne fonctionne pas, l'application déployée présente une erreur. 

## Benchmark
- L'image docker fait 1,27 GB
- Avec locust, on peut voir que le modèle est viable jusqu'à environ 110 utilisateurs
